import json
import pickle
from websockets import ConnectionClosedError
import redis
import Message
import logger
import MessageSplitter


class ClientInfo:
    """
    Manage Client socket
    Keeps a list of online users
    Handles messages for offline users
    Dispatch Messages
    """

    clients = dict()  # online users
    redisDB = redis.Redis(host="redis",
                          port=6379)  # messages waiting for destination to come online

    def __init__(self, client_socket):
        self.socket = client_socket

        # public key will be set after handshaking
        self.public_key = None
    @staticmethod
    async def spawn(socket, path):
        """
        Create a new ClientInfo object for the give socket
        Greet with accepted message
        start listening for any incoming messages
        """

        #logger.myLogger("[Server] New connection")

        data = {
            'type': "accepted",
            'payload': "[Server] Connected"
        }

        _client = ClientInfo(socket)

        if not await _client.send(data):
            await _client.receive_data()

    async def send_error(self, msg):
        """
        Report an error to the client
        """
        error = {
            'type': 'error',
            'details': msg
        }

        await self.send(error)

    async def send(self, msg):
        """
        Sends the given msg, with appropriate headers
        """

        flag = False
        try:
            # convert to json
            string_json = json.dumps(msg)

            # create header for each message
            header = MessageSplitter.create_header(string_json)

            # attach the header
            send_data = header + string_json

            # send
            await self.socket.send(send_data)

            # log each sent message to client
            #logger.myLogger("[Server] send_data" + send_data)

        except:
            #logger.myLogger("[Server] Error sending message")
            flag = True
        return flag

    async def send_pending(self):
        # check for any pending messages for the newly connected client
        if self.public_key in str(ClientInfo.redisDB.keys()):

            #logger.myLogger("[Server] Gathering Pending Messages !")

            # send any pending messages back to the client
            storeableArr = ClientInfo.redisDB.get(self.public_key)
            messages = pickle.loads(storeableArr)
            for msg in messages:
                data = {
                    'type': "msg",
                    'sender': msg.sender,
                    'payload': msg.msg
                }
                await self.send(data)

            # delete the pending messages queue
            ClientInfo.redisDB.delete(self.public_key)

    async def receive_data(self):
        """
        Recursively wait for new data
        take appropriate action based on the incoming message type
        """
        try:
            # wait for new data
            raw_data = await self.socket.recv()

            # decode based on headers
            messages = MessageSplitter.decode_raw_message(raw_data)

            # parse each decoded message
            for received_data in messages:

                # convert json to python dict
                decoded_data = json.loads(received_data)

                # initial handshaking
                if decoded_data["type"] == "connect":

                    # set public key
                    self.public_key = decoded_data["me"]

                    # add to online client
                    ClientInfo.clients[self.public_key] = self
                    await self.send_pending()

                # new message send
                elif decoded_data["type"] == "send":

                    # check if destination is online
                    if decoded_data["destination"] in ClientInfo.clients:

                        # dispatch the message right away

                        #logger.myLogger("[Server] Dispatching Message !")

                        data = {
                            'type': "msg",
                            'sender': self.public_key,
                            'payload': decoded_data["msg"]
                        }
                        await ClientInfo.clients[decoded_data["destination"]].send(data)
                    else:

                        # if the destination is not online, save the message internally
                        # and dispatch when destination comes online

                        #logger.myLogger("[Server] Destination is Offline !")

                        # create a queue for the destination, if not any
                        if decoded_data["destination"] not in str(ClientInfo.redisDB.keys()):
                            storableArr = pickle.dumps([])
                            ClientInfo.redisDB.set(decoded_data["destination"], storableArr)

                        # add the message to the queue
                        storableArr = ClientInfo.redisDB.get(decoded_data["destination"])
                        messages = pickle.loads(storableArr)
                        messages.append(Message.Message(self.public_key,
                                                        decoded_data["msg"],
                                                        decoded_data["destination"]))
                        storableArr = pickle.dumps(messages)
                        ClientInfo.redisDB.set(decoded_data["destination"], storableArr)

            # listen for new data
            await self.receive_data()

        except ConnectionClosedError:
            #logger.myLogger("[Server] Socket closed unexpectedly")

            # client connection is closed, remove it from online clients
            if self.public_key in ClientInfo.clients.keys():
                del ClientInfo.clients[self.public_key]

        except Exception as e:
            # if any error happens, report it to the client
            #logger.myLogger("[Server] " + str(e))

            await self.send_error(str(e))
