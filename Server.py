import socket
import asyncio
import websockets
import ClientInfo
import logger


asyncio.set_event_loop(asyncio.new_event_loop())
host = "127.0.0.1"  # "localhost ip"
port = 9999
start_server = websockets.serve(ClientInfo.ClientInfo.spawn, host, port)
asyncio.get_event_loop().run_until_complete(start_server)
logger.myLogger("[Server] System up and running")
asyncio.get_event_loop().run_forever()

