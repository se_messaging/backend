import logging


def myLogger(message):
    return
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(filename="logs.text", format=FORMAT, level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    logging.getLogger().addHandler(logging.StreamHandler())
    logger.info(message)
