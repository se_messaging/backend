import json


def create_header(json_string):
    msg_len_str = str(len(json_string))
    header = ""

    # create header for each message
    for i in range(10 - len(msg_len_str)):
        header += " "
    header += msg_len_str
    return header


def decode_raw_message(raw_msg):
    msgs = []
    _data = raw_msg

    while len(_data) > 0:
        first_header = _data[:10]
        the_len = int(first_header)
        new_msg = _data[10:the_len + 10]
        msgs += [new_msg]
        _data = _data[the_len + 10:]

    decoded = []

    for msg in msgs:
        decoded_data2 = json.loads(msg)
        decoded += [decoded_data2]

    return msgs
