import asyncio
import json
import websockets
import ClientInfo
import MessageSplitter
import uuid


async def connect_client(host, port, client_hash):
    uri = "ws://%s:%d" % (host, port)

    # c1 connect
    c1 = await websockets.connect(uri)

    # handshake
    hand_shake = {
        'type': "connect",
        'me': client_hash}
    string_hand_shake = json.dumps(hand_shake)
    header = MessageSplitter.create_header(string_hand_shake)
    send_data = header + string_hand_shake

    await c1.send(send_data)

    # wait for handshake response
    data = await c1.recv()

    for _msg in MessageSplitter.decode_raw_message(data):

        msg_decoded = json.loads(_msg)

        if msg_decoded['type'] == "accepted":
            return c1
    return None


# scenario: c1 connects, c2 connects, c2 send msg, c1 receives msg
async def case1(msg, c1_hash, c2_hash):
    async def _send(ws, destination):
        send_msg = {
            'type': "send",
            'destination': destination,
            'msg': msg
        }
        json_string = json.dumps(send_msg)
        header = MessageSplitter.create_header(json_string)
        json_string = header + json_string

        await ws.send(json_string)

    # open server
    port = 9999
    host = "127.0.0.1"

    server = await websockets.serve(ClientInfo.ClientInfo.spawn, host, port)

    uri = "ws://%s:%d" % (host, port)

    # c1 connect
    c1 = await connect_client(host, port, c1_hash)

    # c2 connect
    c2 = await connect_client(host, port, c2_hash)

    # c2 sends test message to c1
    await _send(c2, c1_hash)

    # wait for c1 to receive the message
    test_message_data = await c1.recv()

    for _msg_test_msg_data in MessageSplitter.decode_raw_message(test_message_data):
        decoded_test_message = json.loads(_msg_test_msg_data)
        assert decoded_test_message['payload'] == msg

    server.close()


# scenario: c1 connects, c1 send msg to c2, c2 connects, c2 receives msg from c1
async def case2(msg, c1_hash, c2_hash):
    async def _send(ws, destination):
        send_msg = {
            'type': "send",
            'destination': destination,
            'msg': msg
        }
        json_string = json.dumps(send_msg)
        header = MessageSplitter.create_header(json_string)
        json_string = header + json_string

        await ws.send(json_string)

    # open server
    port = 9999
    host = "127.0.0.1"

    server = await websockets.serve(ClientInfo.ClientInfo.spawn, host, port)

    uri = "ws://%s:%d" % (host, port)

    # c1 connect
    c1 = await connect_client(host, port, c1_hash)

    # c1 sends test message to c2
    await _send(c1, c2_hash)

    # c2 connect
    c2 = await connect_client(host, port, c2_hash)
    # wait for c2 to receive the message
    test_message_data = await c2.recv()

    for _msg_test_msg_data in MessageSplitter.decode_raw_message(test_message_data):
        decoded_test_message = json.loads(_msg_test_msg_data)
        assert decoded_test_message['payload'] == msg

    server.close()


# scenario: c1 connects, c1 send msg to c2, Server reruns, c2 connects, c2 receives msg from c1 (testing redis DB)
async def case3(msg, c1_hash, c2_hash):
    async def _send(ws, destination):
        send_msg = {
            'type': "send",
            'destination': destination,
            'msg': msg
        }
        json_string = json.dumps(send_msg)
        header = MessageSplitter.create_header(json_string)
        json_string = header + json_string

        await ws.send(json_string)

    # open server
    port = 9999
    host = "127.0.0.1"

    server = await websockets.serve(ClientInfo.ClientInfo.spawn, host, port)

    uri = "ws://%s:%d" % (host, port)

    # c1 connect
    c1 = await connect_client(host, port, c1_hash)

    # c1 sends test message to c2
    await _send(c1, c2_hash)

    # close server and run it again
    server.close()
    server = await websockets.serve(ClientInfo.ClientInfo.spawn, host, port)

    # c2 connect
    c2 = await connect_client(host, port, c2_hash)
    # wait for c2 to receive the message
    test_message_data = await c2.recv()

    for _msg_test_msg_data in MessageSplitter.decode_raw_message(test_message_data):
        decoded_test_message = json.loads(_msg_test_msg_data)
        assert decoded_test_message['payload'] == msg

    server.close()


tests = [case1, case2, case3]
myfile = open("send.txt", "r")
messages = myfile.read().split("EEOOTT")


async def _runner():
    # todo: write any test cases that comes to mind
    for test in tests:
        for msg in messages:
            await test(msg, str(uuid.uuid4()), str(uuid.uuid4()))
        print("[X] Test Case #%d Passed" % (tests.index(test) + 1))
    my = open("res.txt", "w")
    my.write("test has been done !")
    my.close()

def test():
    asyncio.run(_runner())

